<?php
namespace App\Service;


use App\Library\MostPopularYouTube;
use App\Service\Paginate;
use Illuminate\Support\Facades\Cache;
class FetchData
{
    private $pagination;

    private $popular_videos;

    public function __construct(Paginate $pagination, MostPopularYouTube $popular_videos)
    {
        $this->pagination = $pagination;
        $this->popular_videos = $popular_videos;
    }

    public function fetch($perPage)
    {
        try {
            $cache = Cache::get('data_per_page_' . $perPage);

            if (empty($cache)) {

                $cache_data = Cache::remember('data_per_page_' . $perPage, 100, function () use ($perPage) {

                    $result = $this->popular_videos->getPopular();

                    $data =  $this->pagination->paginate($result,$perPage);

                    return $data;

                });

                return $cache_data;
            }

            return $cache->toJson();

        } catch (\Exception $exception) {

            return response()->json(['Exception' => $exception->getMessage()], 400);
        }
    }

}