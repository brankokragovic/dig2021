# A few commands for easy navigate

 * sudo docker-compose up
 * sudo docker exec -it dig_php_1 bash
 * cd /code
 * composer update
 

## Left the api key in .env.example 

 * cp .env.example .env
 
## Localhost

 * Localhost is mapped on 1111 port

 * example: 'http://localhost:1111/getData/1'

